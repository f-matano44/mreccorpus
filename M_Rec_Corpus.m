% Copyright 2022 Fumiyoshi MATANO
% Apache License Version 2.0, January 2004

function M_Rec_Corpus() % メイン関数

    % 各種変数 --------------------------------------------
    Fs = 48000; % サンプリングレート
    nBits = 16; % 量子化ビット数
    NumChannnels = 1; % チャンネル数 (1 = monoral)
    corpath = 'script.txt'; % コーパスのファイルパス

    % ここから下は処理 ---------------------------------------

    % 台本を string 型の cell 配列として読み込み
    try
        corpus_str = fileread(corpath);
    catch % コーパスが存在しなかった場合
        disp('---');
        disp('error: コーパスが見つかりませんでした');
        disp('error: mRecCorpus.m を編集して正しいパスを指定してください');
        disp('---');
        return

    end
    %% 正しく読み込めた場合，配列に格納
    corpus_str = sprintf(corpus_str); % char -> string
    corpus_str = strsplit(corpus_str, '\n'); % 改行で分割して cell 配列に格納

    % 収録する番号の確認
    try
        startnum = uint32(input('何番からの音声を録音しますか？: ')); % ここから
        stopnum = uint32(input('何番までの音声を録音しますか？: ')); % ここまで
    catch % 入力値が何かしら異常な場合
        disp('---');
        disp('error: 半角数字でコーパス番号を入力してください');
        disp('---');
        return
    end

    % 入力値が半角数字であった場合，数字に対するチェックを行う．
    %% 指定されたコーパス番号が 0 以下の場合
    if  ~isscalar(startnum) || ~isreal(startnum) || startnum <= 0 || ...
        ~isscalar(stopnum)  || ~isreal(stopnum)  || stopnum <= 0
        disp('---');
        disp('error: コーパス番号は 1 以上の整数を指定してください');
        disp('---');
        return
    elseif startnum > stopnum % start が stop よりも大きい場合
        disp('---');
        disp('error: 指定したコーパス番号を再度確認してください');
        disp('---');
        return
    else
        disp('---');
    end

    % 指定された範囲を録音
    for recnum = startnum:stopnum
        % コーパスが正しくロードされた場合
        if recnum > size(corpus_str, 2) % 指定範囲がコーパスの最大数を超えていた場合
            disp('error: コーパスの最大数を超えています');
            disp('error: 指定したコーパス番号を再度確認してください');
            disp('---');
            return
        end

        % オーディオオブジェクトの新規作成
        recObj = audiorecorder(Fs, nBits, NumChannnels);

        % 録音ループ
        question = 'xxx'; % question 変数の初期化
        while ~strcmp(question, 'y')
            % オーディオオブジェクトの上書き
            recObj = audiorecorder(Fs, nBits, NumChannnels);

            % 読み上げる台本の表示
            disp(corpus_str{recnum});

            % 録音
            input('Enter で録音開始', 's');
            record(recObj); % 録音開始
            input('録音中......Enter で録音終了します', 's');
            stop(recObj); % 録音終了

            % 再生
            fprintf('再生中......');
            pause(1);
            playblocking(getplayer(recObj)); % 録音した音声の再生

            % CHECK
            question = 'xxx'; % question 変数の初期化
            while ~(strcmp(question, 'y') || strcmp(question, 'n'))
                question = input('この音声を保存しても良いですか？[y/n]: ', 's');
            end
            disp('---');
        end

        % 録音データの保存
        y = getaudiodata(recObj);
        savepath = sprintf('wav/CORPUS_%04d.wav', recnum);
        audiowrite(savepath, y, Fs);
    end

end
