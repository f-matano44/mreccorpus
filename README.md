# mRecCorpus
MATLAB Recorder for Corpus<br>
↓ダウンロードはこちら↓<br>
[![Latest Release](https://gitlab.com/f-matano44/mreccorpus/-/badges/release.svg)](https://gitlab.com/f-matano44/mreccorpus/-/releases)

## Description
Q: これはなに？<br>
A: コーパス作成に特化した CLI ベースの録音スクリプト<br>
初期設定では以下の設定で録音を行います．
* チャンネル: モノラル
* ビットレート: 16bit
* サンプリングレート: 48kHz
* 保存形式: WAV

## Installation
このプログラムは matlab script で記述されていますので，お使いになる方は MATLAB もしくは GNU octave (以下，octave) をインストールしてからご利用ください．
* MATLAB（有料）: https://jp.mathworks.com/products/matlab.html
* octave（無料）: https://octave.org/download

また，動作確認バージョンは以下の通りです．
* MATLAB: R2022a
* octave GUI: 7.2.0

## Directory structure
* wav/ ← 保存先
* mRecCorpus.m ← アプリケーション本体
* LICENSE ← このプログラムのライセンス
* README.md ← これ
* script.txt ← 録音したいコーパス台本

## Usage
MATLAB もしくは octave を起動し，左側に表示されているであろうエクスプローラを操作して `mRecCorpus` フォルダに入り，以下の通り実行してください．色々指示が出ますので，それに従えば録音できるかと思います．入力は全て半角英数でお願いします．<br>

![使用例](docs/images/octave-ss.png)

## Authors and acknowledgment

### mRecCorpus
* Fumiyoshi MATANO
  
### JSUT コーパス
* 園部 良介 (東京大学) 
* 高道 慎之介 (東京大学)
* 猿渡 洋 (東京大学)

## License

### mRecCorpus
* Apache License Version 2.0, January 2004

### JSUT コーパス
* wikipedia (https://ja.wikipedia.org/) CC-BY-SA 3.0 
* TANAKA corpus (http://www.edrdg.org/wiki/index.php/Tanaka_Corpus) CC-BY 2.0
* Our original sentences. CC-BY-SA 4.0
